package com.baplay.controller.api;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.constants.Config;
import com.baplay.controller.BaseController;
import com.baplay.dao.SMSRecordDao;
import com.baplay.dto.SMSRecord;
import com.baplay.utils.HttpUtil;
import com.baplay.utils.MD5Util;
import com.baplay.utils.SMSUtils;








@Controller
@Scope("request")
public class SendSMSApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(SendSMSApi.class);
	
	@Autowired
	private SMSRecordDao smsRecordManager;
	
	
	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssS");
	 
	private final String KEY = "0af8ab03edbbaa9afbc949f2f1ddd285";
	private final String platform = "RC";
	private final String reportUrl = "http://www.raidcall.com.tw/api/sms/result.php";
	@RequestMapping(value = "sendSms", produces = "application/json", method=RequestMethod.POST)
	public @ResponseBody Map send(@RequestParam("mobile") String mobile,
			@RequestParam("content") String content,
			@RequestParam("token") String token,
			@RequestParam(value = "validSec", required = false, defaultValue = "7200") String validSec,
			@RequestParam(value = "serialNo", required = false, defaultValue = "") String serialNo)
	{	
		LOG.info("============== SendSMSApi sendSms ====================");
		LOG.info("mobile="+mobile);
		LOG.info("content="+content);
		LOG.info("validSec="+validSec);
		LOG.info("serialNo="+serialNo);
		LOG.info("token="+token);
		String code = Config.RETURN_SUCCESS;
		Map result = new HashMap();
		if (!token.equalsIgnoreCase(MD5Util.crypt(mobile+KEY))) {
			code = Config.RETURN_TOKEN_FAIL;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			LOG.info("result="+result);
			return result;
		}
		try {
			 HashMap<String, String> retHM = this.sendSMS(platform, content,
						mobile, serialNo, validSec);
			String theSmsRetStatus = retHM.get("status");

			if (theSmsRetStatus == null || 
				!theSmsRetStatus.equals("0") || 
				retHM.get("MessageID") == null) 
			{
				code = Config.RETURN_SYSTEM_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				LOG.info("result="+result);
				return result;

			}
			
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			result.put("smsId", retHM.get("MessageID"));
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	
	 
		 
	 private HashMap<String, String> sendSMS(String platform,
				String msg, String mobile, String sno, String validSec) {
		    if (mobile.startsWith("+88609")) {
		    	mobile = mobile.substring(4);
//		    } else if (mobile.startsWith("+852") || mobile.startsWith("+853")) {
//		    	mobile = mobile.substring(1);
		    }
			SMSUtils sms = new SMSUtils();
			String smsRetStr = sms.sendSMS(
					false,  //測試環境請設 true
					mobile,
					msg,
					platform, 
					(StringUtils.isBlank(sno))? platform + sdf.format(new Date()) : sno,
					validSec);
			LOG.info("sendSMS smsTestRet:" + smsRetStr);
			HashMap<String, String> retHM = SMSUtils.getHashMapFromStr(smsRetStr);

			String retStatus = retHM.get("status");
//				if (retStatus != null) {
				LOG.info("sendSMS retStatus="+retStatus);
//					if (retStatus.equals("0")) {
					SMSRecord smsRecord = new SMSRecord();
					smsRecord.setMessageId(retHM.get("MessageID"));
					smsRecord.setUsedCredit(retHM.get("UsedCredit"));
					smsRecord.setMemberId(retHM.get("MemberID"));
					smsRecord.setCredit(retHM.get("Credit"));
					smsRecord.setMobileNo(mobile);
					smsRecord.setStatus(retHM.get("status"));
					smsRecord.setCreateTime(new Timestamp(System
							.currentTimeMillis()));

					smsRecordManager.add(smsRecord);
//					}
//				}

			return retHM;
		}

	 
	// 接收 yoyo8 簡訊公司的回call
		@RequestMapping(value = "/reportSMS")
		public String reportSMS(
				@RequestParam(value = "status") String status,
				@RequestParam(value = "MemberID", required = false) String memberID,
				@RequestParam(value = "MessageID", required = false) String messageID,
				@RequestParam(value = "UsedCredit", required = false) String usedCredit,
				@RequestParam(value = "Credit", required = false) String credit,
				@RequestParam(value = "MobileNo", required = false) String mobileNo,
				@RequestParam(value = "SourceProdID", required = false) String sourceProdID,
				@RequestParam(value = "SourceMsgID", required = false) String sourceMsgID) throws Exception {
			LOG.info("============== SendSMSApi reportSMS ====================");
			LOG.info("status="+status);
			LOG.info("MemberID="+memberID);
			LOG.info("MessageID="+messageID);
			LOG.info("UsedCredit="+usedCredit);
			LOG.info("Credit="+credit);
			LOG.info("MobileNo="+mobileNo);
			LOG.info("SourceProdID="+sourceProdID);
			LOG.info("SourceMsgID="+sourceMsgID);


//			if (status.equals("0")) {
				if (messageID != null) {
					SMSRecord retRecord = smsRecordManager
							.findByMessageId(messageID);
					if (sourceMsgID == null) {
						sourceMsgID = "";
					}
					if (retRecord != null) {
						Timestamp nowTime = new Timestamp(
								System.currentTimeMillis());
						System.out.println("getReport " + messageID + " " + status
								+ " " + sourceMsgID + " " + nowTime);
						smsRecordManager.updateReport(messageID, status,
								sourceProdID, sourceMsgID, nowTime);
					}
				}

//			}
			//TODO call url and rety 3 times
//			StringBuffer sb = new StringBuffer();
//			sb.append("smsId=").append(messageID).append("&mobile=").append(mobileNo)
//				.append("&status=").append(status).append("&token=").append(MD5Util.crypt(messageID+KEY));
			NameValuePair[] urlParameters = new NameValuePair[4];
			urlParameters[0]=new NameValuePair("smsId",		messageID);
			urlParameters[1]=new NameValuePair("mobile",		mobileNo);
			urlParameters[2]=new NameValuePair("status",		status);
			urlParameters[3]=new NameValuePair("token",		MD5Util.crypt(messageID+KEY));	
			for (int i=0; i<3; i++) {
//				String result = HttpUtil.sendPost(reportUrl, sb.toString());
				String result = HttpUtil.sendRequest(reportUrl, "post", urlParameters);
				if (result.indexOf("0000")>-1) {
					break;
				} else {
					Thread.sleep(5000);
				}	
			}
			String nextPage = "/reportSMS";
			return nextPage;
		}
	
	public static void main(String[] arg) {
		
		System.out.println(MD5Util.crypt("rcsms"));
	}
		
	
}
